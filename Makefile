.PHONY: run clean
.DEFAULT_TARGET: main.pdf

main.pdf: $(shell find -type f -iname "*.tex")
	git archive --format=zip --output=source-code.zip --worktree-attributes -9 HEAD
	latexmk -pdf main.tex

run: main.pdf
	evince main.pdf &

clean:
	@for PART in $(shell find -type f -iname "*.tex" | sed 's:.tex::g'); do \
		echo "*.out *.nav *.aux *.toc *.log *.snm *.pdf *.vrb *.fdb_latexmk *.fls" | sed "s:*:$$PART:g" | xargs rm -f; \
	done

